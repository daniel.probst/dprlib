% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/dprlib_gbsm.R
\name{gbsm_option}
\alias{gbsm_option}
\title{Generalised Black Scholes Merton Option Price and Greeks}
\usage{
gbsm_option(CallFlag, S, X, Time, r, b, sigma, ops = c("Premium"))
}
\arguments{
\item{CallFlag}{logical(vector), TRUE for call, put otherwise}

\item{S}{numeric(vector), the underlying price}

\item{X}{numeric(vector), the strike}

\item{Time}{numeric(vector), in time units (typically years)}

\item{r, }{numeric(vector), interest rate e.g. 0.01 for 1 percent}

\item{b, }{numeric(vector) GBSM parameter.
b=r: options on non-dividend paying stock.
b=r-q: options on stock or index paying a yield of q.
b=0: options on futures. b= r-rf currency options,
rf is 2nd currency rate.}

\item{sigma, }{the volatility per Time}

\item{ops}{character(vector) Operations: Premium, Delta, Gamma, Theta}
}
\description{
Generalised Black Scholes Merton Option Price and Greeks
}
