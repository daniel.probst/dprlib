#' Plot a candle plot
#'
#' @param x time series, OHLC time series
#' @export candle_chart
#' @importFrom dplyr mutate .data
#' @importFrom ggplot2 ggplot aes geom_boxplot
#' @importFrom quantmod has.OHLC Op Hi Lo Cl
#' @importFrom rlang .data
#' @importFrom zoo index
candle_chart <- function(x) {
  if (!all(quantmod::has.OHLC(x))) {
    stop("Input should be OHLC.")
  }
  open <- as.vector(quantmod::Op(x))
  high <- as.vector(quantmod::Hi(x))
  low <- as.vector(quantmod::Lo(x))
  close <- as.vector(quantmod::Cl(x))
  mydf <- data.frame(Date = zoo::index(x),
                     Open = open, High = high, Low = low, Close = close)
  mydf <- dplyr::mutate(mydf, CandleLower = pmin(.data$Open, .data$Close),
                  CandleUpper = pmax(.data$Open, .data$Close),
                  CandleMiddle = (.data$CandleLower + .data$CandleUpper) / 2,
                  Fill = ifelse(.data$Open <= .data$Close, "#00BFC4", "#F8766D"))
  g <- ggplot(mydf, aes(x = .data$Date, fill = I(.data$Fill))) +
    geom_boxplot(stat = "identity",
                 aes(group = .data$Date, lower = .data$CandleLower, middle = .data$CandleMiddle,
                     upper = .data$CandleUpper, ymin = .data$Low, ymax = .data$High))
  return(g)
}

#' plot 1 x-axis vs multiple panels/facets
#'
#' @param df data frame
#' @param x numeric, column index of x variable
#' @param panels, list, list of vetors of  indices to be plotted per panel
#' @param GGPLOT, bool, return plot instead of data.frame
#' @export ggpanel
#' @importFrom ggplot2 ggplot aes geom_line facet_grid
#' @importFrom rlang .data
#' @importFrom tidyr pivot_longer
ggpanel <- function(df, x, panels, GGPLOT=FALSE) {
  retdf <- data.frame()
  for (i in panels) {
    pnm <- paste(colnames(df)[i], sep = "", collapse = ";")
    tmpdat <- data.frame(Index = df[, x],
                         Panel = rep(pnm, nrow(df)),
                         df[, colnames(df)[i]])
    tmpmdat <- tidyr::pivot_longer(data = tmpdat,
                                   cols = !c(.data$Index, .data$Panel),
                                   names_to = "Variable",
                                   values_to = "Value")
    retdf <- rbind(retdf, tmpmdat)
  }
  if (!GGPLOT) {
    colnames(retdf)[1] <- colnames(df)[x]
    return(retdf)
  } else {
    P <- ggplot(data = retdf,
                aes(x = .data$Index, y = .data$Value,
                    colour = .data$Variable)) +
      geom_line() +
      facet_grid(.data$Panel~., scales = "free_y") +
      labs(x = colnames(df)[x], y = NULL)
  }
  return(P)
}
