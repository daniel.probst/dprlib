# dprlib
My personal library of R functions which may or may not be useful for quantitative commodity trading. 

# Installing

## From within R

Execute the following from within R:

`library(devtools)`

`devtools::install_git(url = "https://gitlab.com/daniel.probst/dprlib.git")`

## From the command line

Clone the repository and cd to wherever you cloned it to, there execute in you (linux) shell:

`./dpbuild.sh`

This will execute R and use devtools to build the package, which can then be installed from within R.

# Generalised Black Scholes Merton Option Pricing Formulas

The option pricing formulas are from the Collector's book (ref) and the functions are implemented with following focus:
* vectorisation (input can be vectors)
* speed
* tidyverse compatible: vectorised functions can be used in dplyr::mutate 
